#language: pt

Funcionalidade: Cadastro
    Sendo um músico que possui equipamentos musicais
    Quero fazer o meu cadastro no Rocklov
    Para que eu possa disponibilizá-los para locação

    @cadastro
    Cenário: Fazer cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome               | email                   | senha      |
            | Emanuelle Oliveira | emanuelleol@outlook.com | manu102030 |
        Então sou redirecionado para o Dashboard

    Esquema do Cenario: Tentativa de Cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome         | email         | senha         |
            | <nome_input> | <email_input> | <senha_input> |
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | nome_input         | email_input             | senha_input | mensagem_output                  |
            |                    | emanuelleol@outlook.com | abc102030   | Oops. Informe seu nome completo! |
            | Emanuelle Oliveira |                         | manu102030  | Oops. Informe um email válido!   |
            | Emanuelle Oliveira | emanuelleol&outlook.com | manu102030  | Oops. Informe um email válido!   |
            | Emanuelle Oliveira | emanuelleol*outlook.com | manu102030  | Oops. Informe um email válido!   |
            | Emanuelle Oliveira | emanuelleol@outlook.com |             | Oops. Informe sua senha secreta! |



