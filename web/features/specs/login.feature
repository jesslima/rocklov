#language: pt

Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais
    
    @login
    Cenário: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "blairwaldorf@gmail.com" e "manu102030"
        Então sou redirecionado para o Dashboard

    Esquema do Cenário: Tentar Logar

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | email_input           | senha_input | mensagem_output                 |
            | blairwaldorf@gmail.com| manu1020    | Usuário e/ou senha inválidos.   |
            | italomoura@outlook.com| manu102030  | Usuário e/ou senha inválidos.   |
            | blairwaldorf&gmail.com| manu1020    | Oops. Informe um email válido!  |
            |                       | manu1020    | Oops. Informe um email válido!  |
            | blairwaldorf@gmail.com|             | Oops. Informe sua senha secreta!|


    